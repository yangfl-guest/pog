#!/bin/sh
set -e

mkdir build
cd build
cmake /usr/share/doc/libpog-dev/examples/
make
cd ..
rm -rf build
